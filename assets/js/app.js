// DEFINE APP
var myApp = angular.module('myApp', ['ui.bootstrap']);

// MAIN CONTROLLER
myApp.controller('mainController', ['$scope','$modal', function ($scope, $modal) {

    $scope.results = [
    	{
            title: "Digital Graphx Logo",
            tags: "design",
            description: "Digital Graphx was created back in 2012. I was fresh out of my undergraduate and I needed a brand identify for my freelance work.",
            image: "assets/img/thumbnail-digital-graphx-logo.jpg",
            action: "Behance Link",
            fa: "fa-behance",
            link: "http://www.behance.net/gallery/40167357/Digital-Graphx-Logo",
            tech: ["Adobe Illustrator"]
        },
        {
            title: "Palindrome Checker",
            tags: "web",
            image: "assets/img/thumbnail-palindrome.jpg",
            description: "Inspired by my tech interview with IBM Digital. I was asked to use JavaScript to confirm if a string was a palindrome.",
            action: "External Link",
            fa: "fa-external-link",
            link: "http://palindrome.digitalgraphx.net",
            tech: ["CSS3","JavaScript", "Sass"]
        },
        {
            title: "Find A Designer",
           	tags: "web",
            image: "assets/img/thumbnail-find-a-designer.jpg",
            description: "This project was created to showcase the AngularJS Filter feature.",
            action: "External Link",
            fa: "fa-external-link",
            link: "http://find.digitalgraphx.net",
            tech: ["HTML5","CSS3","Sass","AngularJS"],
        },
        {
            title: "Pink Papaya",
            tags: "marketing",
            description: "This was a project I did to help my wife and her direct sales business",
            image: "assets/img/thumbnail-pink-papaya-marketing.jpg",
            action: "View PDF",
            fa: "fa-file-pdf-o",
            link: "assets/docs/pink-papaya-material.pdf",
            tech: ["Adobe Illustrator"]
        },
        {
            title: "Promotional Site",
            tags: "web",
            image: "assets/img/thumbnail-shyanne-ward-com.jpg",
            description: "This is a site I created for my wife so she would have an online presents for her direct sales business",
            action: "External Link",
            fa: "fa-external-link",
            link: "http://www.shyanneward.com",
            tech: ["HTML5","CSS3","Bootstrap","Sass", "Adobe Illustrator"],
        },
        {
            title: "Watson Wallpaper",
            tags: "design",
            description: "I did this project because I wanted a pixel perfect wallpaper for my Lenovo T430 workstation.",
            image: "assets/img/thumbnail-watson-wallpaper.jpg",
            action: "View Image",
            fa: "fa-image",
            link: "assets/img/full-watson-wallpaper.jpg",
            tech: ["Adobe Photoshop"]
        },
        {
            title: "WarZonE Sports",
            tags: "design",
            image: "assets/img/thumbnail-wze.jpg",
            description: "This is a logo that I created for a local sports orginazations.",
            action: "Behance",
            fa: "fa-behance",
            link: "http://www.behance.net",
            tech: ["HTML5","CSS3","Less", "Adobe Illustrator"]
        },
        {
            title: "Digital Graphx Site",
           	tags: "web",
            image: "assets/img/thumbnail-digital-graphx-net.jpg",
            description: "This is the main website for Digital Graphx",
            action: "External Link",
            fa: "fa-external-link",
            link: "http://www.digitalgraphx.net",
            tech: ["HTML5","CSS3","AngularJS","Node.js","Grunt","Atom", "Adobe Illustrator"]
        },
        {
            title: "Maple Valley",
            tags: "design",
            image: "assets/img/thumbnail-mv.jpg",
            description: "I created this logo because I felt my high school's current logo was dated. ",
            action: "Behance",
            fa: "fa-behance",
            link: "https://www.behance.net/gallery/42035197/New-Maple-Valley-Logo",
            tech: ["Adobe Illustrator"]
        },
        {
            title: "A Plus Services",
            tags: "design",
            image: "assets/img/thumbnail-aplus.jpg",
            description: "I created this logo for my mother's commercial cleaning company",
            action: "View PDF",
            fa: "fa-file-pdf-o",
            link: "assets/docs/aplus-branding.pdf",
            tech: ["Adobe Illustrator"]
        },
    ];
   
// EMPLOYMENT DATA
    $scope.employment = [
    	{
    		employer: "International Business Machines",
    		image: "assets/img/thumbnail-employment-ibm.jpg",
    		position: "Front End Developer",
    		duration: "Jul 2013 - Current",
    		description: "I've really enjoyed my time at IBM. Great company to work for with a rich history.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.ibm.com"
    	},
    		{
    		employer: "Best Buy 803",
    		image: "assets/img/thumbnail-employment-best-buy.jpg",
    		position: "Mobile Sales",
    		duration: "Oct 2014 - Jan 2015",
    		description: "Though I was still working at IBM, I decided to go back to Best Buy for some seasonal work.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.bestbuy.com"
    	},
    	{
    		employer: "Olivet College",
    		image: "assets/img/thumbnail-employment-olivet-college.jpg",
    		position: "Manager, Designer",
    		duration: "Nov 2008 - May 2012",
    		description: "I really enjoyed working at Olivet College. Great environment and great people.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.olivetcollege.edu"
    	},
    	{
    		employer: "Best Buy 416",
    		image: "assets/img/thumbnail-employment-best-buy.jpg",
    		position: "Customer Service",
    		duration: "Apr 2011 - Oct 2011",
    		description: "What I loved about this job was the constant interation with customers.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.bestbuy.com"
    	},
    	{
    		employer: "Meijer",
    		image: "assets/img/thumbnail-employment-meijer.jpg",
    		position: "Grocery Stocker",
    		duration: "Apr 2008 - June 2008",
    		description: "I stocked grocery, dairy and frozen.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.meijer.com"
    	},
    	{
    		employer: "Good Time Pizza",
    		image: "assets/img/thumbnail-employment-good-time.jpg",
    		position: "Pizza Cook",
    		duration: "Apr 2006 - Oct 2007",
    		description: "My first payroll job. Started as a dishwasher, finished as a Pizza Master!",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.facebook.com/pages/Good-Time-Pizza/137459812963701"
    	},
    ];
    
// EDUCATION DATA
    $scope.education = [
    	{
    		school: "University of Illinois",
    		image: "assets/img/thumbnail-edu-illinois.jpg",
    		degree: "Master of Busness Administration",
    		field: "Marketing",
    		duration: "Starts Jan 2017.",
    		description: "I've decided to get a MBA in Marketing.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.illinois.edu"
    	},
    	{
    		school: "Full Sail University",
    		image: "assets/img/thumbnail-edu-full-sail.jpg",
    		degree: "Graduate Certificate",
    		field: "Internet Marketing",
    		duration: "Sep 2016 - Dec 2016.",
    		description: "I've decided to get a Graduate Certificate in Internet Marketing.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.fullsail.edu"
    	},
    	{
    		school: "Olivet College",
    		image: "assets/img/thumbnail-edu-olivet-college.jpg",
    		degree: "Bachelor of Arts",
    		field: "Computer Science",
    		duration: "Fall 2008 - Spring 2012",
    		description: "My time at Olivet was great. I learned a lot both inside and outside of the classroom.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "http://www.olivetcollege.edu"
    	},
    	{
    		school: "Maple Valley",
    		image: "assets/img/thumbnail-edu-maple-valley.jpg",
    		degree: "High School Diploma",
    		field: "General Studies",
    		duration: "Sep 2003 - Jun 2007",
    		description: "I miss playing baseball and soccer for the Lions... great times, great memories!",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "http://www.mvs.k12.mi.us"
    	}
    ];
    
// MOOC DATA
    $scope.training = [
    	{
    		site: "edX - MichiganX",
    		image: "assets/img/thumbnail-mooc-ux-design.jpg",
    		series: "XSeries: UX Design",
    		duration: "Sep 2016 - Mar 2017",
    		description: "As a developer, I want to increase my UX skillset.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.edx.org"
    	},
    	{
    		site: "edX - MichiganX",
    		image: "assets/img/thumbnail-mooc-ux-research.jpg",
    		series: "XSeries: UX Research",
    		duration: "Sep 2016 - Mar 2017",
    		description: "As a developer, I want to learn more about the imporantace of UX.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.edx.org"
    	},
    	{
    		site: "edX - UC BerkeleyX",
    		image: "assets/img/thumbnail-mooc-marketing-analytics.jpg",
    		series: "XSeries: Marketing Analytics",
    		duration: "Start Date Pending",
    		description: "As a designer, I want to learn more about marketing my designs.",
    		action: "External Link",
            fa: "fa-external-link",
    		link: "https://www.edx.org"
    	},
    ];
    
// RESUME DATA
    $scope.resumes = [
    	{
    		name: "Technical Resume",
    		image: "assets/img/thumbnail-resume-technical.jpg",
    		description: "In this resume you will find a summary of my technical work.",
    		action: "View PDF",
            fa: "fa-file-pdf-o",
    		link: "https://www.google.com"
    	},
    	{
    		name: "Design Resume",
    		image: "assets/img/thumbnail-resume-design.jpg",
    		description: "In this resume you will find a summary of my technical work.",
    		action: "View PDF",
            fa: "fa-file-pdf-o",
    		link: "https://www.google.com"
    	},
    	{
    		name: "Full Resume",
    		image: "assets/img/thumbnail-resume-full.jpg",
    		description: "In this resume you will find a summary of my technical work.",
    		action: "View PDF",
            fa: "fa-file-pdf-o",
    		link: "https://www.google.com"
    	},
    ];

// FEEDBACK DATA
    $scope.feedback = [
    	{
    		quoteText: '"Based on my experience working with Cody, I would recommend him for a position in testing or evaluating user interfaces."',
    		quoteSource: "- Fred Wu, Ph.D.",
    		sourceRoll:	"Manager, IBM Watson Research",
    		active: "active"
    	},
    	{
    		quoteText: '"Cody is a great combination of technical skills and social/leadership skills."',
    		quoteSource: "- Don Tuski, Ph.D.",
    		sourceRoll:	"President, Olivet College (Former)",
    	},
    	{
    		quoteText: '"Cody asked great questions and helped me with the look I was going for. And so speedy too!"',
    		quoteSource: "- Jill Antczak‎",
    		sourceRoll:	"Premier Team Leader, Pink Papaya"
    	},
    	{
    		quoteText: '"Cody demonstrated his leadership qualities as president of the Olivet College Computer Science Club."',
    		quoteSource: "- Michael Fredericks",
    		sourceRoll:	"Professor, Olivet College"
    	},
    	{
    		quoteText: '"If anyone needs any type of graphics made Cody Ward is your man! Super fast. Professional and amazing quality!"',
    		quoteSource: "- Alesha Holmden",
    		sourceRoll:	"Brand Promoter, Le-Vel"
    	},
    	{
    		quoteText: '"He does amazing work and I\'m so thankful for his patience and resolutions... I highly recommend him!"',
    		quoteSource: "- Heather Mills",
    		sourceRoll:	"Brand Promoter, Le-Vel"
    	},
    ];
}]);

// FOOTER CONTROLLER
myApp.controller('footerController', ['$scope', function ($scope) {
    $scope.social = [
    	{link: "https://www.twitter.com/wardcx90", icon: "fa-twitter"},
    	{link: "https://www.linkedin.com/in/wardcx90", icon: "fa-linkedin"},
    	{link: "https://plus.google.com/u/0/118293986281891953780", icon: "fa-google-plus"},
    	{link: "https://www.facebook.com/cwdigitalgraphx", icon: "fa-facebook"},
    	{link: "https://codepen.io/WardCX90", icon: "fa-codepen"},
    	{link: "https://gitlab.com/u/WardCX90", icon: "fa-gitlab"},
    	{link: "https://stackoverflow.com/users/6267090/wardcx90", icon: "fa-stack-overflow"},
    	{link: "https://www.behance.net/wardcx90", icon: "fa-behance"},
    ];
}]);

// ADD ACTIVE FILTER CLASS
$(document).ready(function(){
    $('.filter-button').click(function(){
        $(this).toggleClass('active-button');
    });
});

// CAROUSEL INTERVAL
$('.carousel').carousel({
  	interval: 1000 * 10
});

// INITIALIZE BOOTSTRAP TOOLTIPS
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

// FLIP TILE
$(document).ready(function(){
    $('.tile').click(function(){
        $(this).toggleClass('flipped');
    });
});